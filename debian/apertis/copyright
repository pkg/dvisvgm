Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: fedorainfracloud.org/coprs/mgieseki/dvisvgm/package/dvisvgm/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/mgieseki/dvisvgm/package/dvisvgm)
 2005-2025, Martin Gieseking <martin.gieseking@uos.de>
License: GPL-3+

Files: debian/*
Copyright: 2019-2024, Hilmar Preusse <hille42@web.de>
License: GPL-2+

Files: doc/*
Copyright: 2005-2024 Martin Gieseking <martin.gieseking@uos.de>
License: GPL-3+

Files: doc/dvisvgm.txt.in
Copyright: 2005-2025, Martin Gieseking <martin.gieseking@uos.de>
License: GPL-3+

Files: libs/*
Copyright: 2001-2019, Peter Selinger.
License: GPL

Files: libs/boost/*
Copyright: no-info-found
License: BSL-1.0

Files: libs/boost/boost-vectorstream.hpp
Copyright: Ion Gaztanaga 2005-2012. Distributed under the Boost
License: BSL-1.0 or NTP

Files: libs/brotli/*
Copyright: 2009, 2010, 2013-2016, the Brotli Authors.
License: Expat

Files: libs/brotli/common/*
Copyright: 2010, 2013-2018, 2022, Google Inc.
License: Expat

Files: libs/brotli/enc/*
Copyright: 2010, 2013-2018, 2022, Google Inc.
License: Expat

Files: libs/brotli/include/*
Copyright: 2010, 2013-2018, 2022, Google Inc.
License: Expat

Files: libs/clipper/*
Copyright: no-info-found
License: BSL-1.0

Files: libs/clipper/clipper.cpp
 libs/clipper/clipper.hpp
Copyright: 2010-2014, Angus Johnson
License: BSL

Files: libs/md5/*
Copyright: 2001, Alexander Peslyak and it is hereby released to the
License: public-domain

Files: libs/variant/*
Copyright: no-info-found
License: BSL-1.0

Files: libs/variant/include/*
Copyright: 2015-2017, Michael Park
License: BSL-1.0

Files: libs/woff2/*
Copyright: no-info-found
License: Apache-2.0

Files: libs/woff2/include/*
Copyright: 2010, 2013-2018, 2022, Google Inc.
License: Expat

Files: libs/woff2/src/*
Copyright: 2010, 2013-2018, 2022, Google Inc.
License: Expat

Files: libs/xxHash/*
Copyright: 2012-2023, Yann Collet
License: BSD-2-clause

Files: m4/*
Copyright: 2009, Tom Howard <tomhoward@users.sf.net>
 2009, Allan Caffee <allan.caffee@gmail.com>
License: FSFAP

Files: m4/ax_check_compile_flag.m4
Copyright: 2011, Maarten Bosmans <mkbosmans@gmail.com>
 2008, Guido U. Draheim <guidod@gmx.de>
License: FSFAP

Files: m4/ax_code_coverage.m4
Copyright: 2015, 2018, Bastien ROUCARIES
 2012, Xan Lopez
 2012, Paolo Borelli
 2012, Dan Winship
 2012, Christian Persch
 2012, 2016, Philip Withnall
License: LGPL-2.1+

Files: m4/ax_cxx_compile_stdcxx.m4
Copyright: 2021, Jörn Heusipp <osmanx@problemloesungsmaschine.de>
 2020, Jason Merrill <jason@redhat.com>
 2019, Enji Cooper <yaneurabeya@gmail.com>
 2016, 2018, Krzesimir Nowak <qdlacz@gmail.com>
 2015, Paul Norman <penorman@mac.com>
 2015, Moritz Klammler <moritz@klammler.eu>
 2014, 2015, Google Inc.; contributedAlexey Sokolov <sokolov@google.com>
 2013, Roy Stogner <roystgnr@ices.utexas.edu>
 2012, Zack Weinberg <zackw@panix.com>
 2008, Benjamin Kosnik <bkoz@redhat.com>
License: FSFAP

Files: m4/ax_file_escapes.m4
Copyright: 2008, Tom Howard <tomhoward@users.sf.net>
License: FSFAP

Files: m4/ax_gcc_builtin.m4
Copyright: 2013, Gabriele Svelto <gabriele.svelto@gmail.com>
License: FSFAP

Files: tests/gtest/*
Copyright: 2003, 2005-2009, 2015, Google Inc.
License: BSD-3-clause

Files: vc/miktex-com/miktex-session.idl
 vc/miktex-com/miktexidl.idl
Copyright: 2006, 2007, Christian Schenk
License: GPL-2+

Files: INSTALL doc/Makefile.am libs/Makefile.am libs/defs.am src/iapi.h src/ierrors.h
Copyright: 2005-2024 Martin Gieseking <martin.gieseking@uos.de>
License: GPL-3+
